from api_interface import coindesk
import string
import random

bpi_data = {}
wallet_balance = {}
dates = {}
h_bpi = coindesk.historical_bpi()




wallet_balance['btc'] = 2

log = open('logs/static_algorithm_' + ''.join(
    random.choice(string.ascii_uppercase + string.digits) for _ in range(5)) + '.txt', 'w')

bpi_data = h_bpi.get_bpi('2017-10-01', '2017-12-31')  # use format YYYY-MM-DD;

# print(bpi_data['bpi'])

# for x in bpi_data['bpi']:
#     print bpi_data['bpi'][x]

dates = bpi_data.keys()

y = 0

int(y)

for x in bpi_data:
    if (y == 0):
        z = x

    btc_in_usd = int(wallet_balance['btc']) * bpi_data[x]


    log.write('**************************************************\n')
    log.write('Date: ' + dates[y] + '\n')
    log.write('Current BPI: ' + str(bpi_data[x]) + '\n')
    log.write('BTC Account Balance: ' + str(wallet_balance['btc']) + ' BTC or ' + str(btc_in_usd) + ' USD\n')
    log.write('Total Balance: ' + str(btc_in_usd) + ' USD\n')
    # log.write('**************************************************\n')

    y+=1

    print'.',

# print(bpi_data[0])

original_balance = int(wallet_balance['btc']) * bpi_data[z]

total_balance_in_usd = int(wallet_balance['btc']) * bpi_data[x]

total_gain_loss = total_balance_in_usd - original_balance

percentage = (abs(original_balance - total_balance_in_usd) / (original_balance + total_balance_in_usd) / 2) * 100

log.write('**************************************************\n')
log.write('/////////////////////Summary//////////////////////\n')
log.write('**************************************************\n')
log.write('Total Account Balance: ' + str(total_balance_in_usd) + ' USD\n')
log.write('Total Balance of BTC Wallet: ' + str(wallet_balance['btc']) + ' BTC\n')
log.write('Total Gain or Loss: ' + str(total_gain_loss) + ' USD\n')
log.write('Percentage of Change: ' + str(percentage) + '%\n')
log.write('**************************************************\n')
log.write('//////////////////////////////////////////////////\n')
log.write('**************************************************')

print "\n\nStatus: program completed"