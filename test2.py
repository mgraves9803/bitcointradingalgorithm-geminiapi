from api_interface import coindesk
import string
import random
import time

bpi_data = {}
wallet_balance = {}
dates = {}
h_bpi = coindesk.historical_bpi()

original_usd = 5000
original_btc = 50

wallet_balance['usd'] = original_usd  # 5,000 USD to start. This is the budget and we will use USD
wallet_balance['btc'] = original_btc  # 50 BTC to start. At the start date 50 BTC = 665.205 USD.

con_amount = raw_input(
    "What is the consistent amount in usd of btc in the btc wallet you wold like?: ")  # the consistent ammount in USD
con_amount = int(con_amount)

log = open('logs/simple_algorithm_' + ''.join(
    random.choice(string.ascii_uppercase + string.digits) for _ in range(5)) + '.txt', 'w')

bpi_data = h_bpi.get_bpi('2017-12-01', '2017-12-25')  # use format YYYY-MM-DD;

# print(bpi_data['bpi'])

# for x in bpi_data['bpi']:
#     print bpi_data['bpi'][x]

dates = bpi_data.keys()

y = 0

int(y)

for x in bpi_data:

    if (y == 0):
        z = x

    btc_in_usd = wallet_balance['btc'] * bpi_data[x]

    usd_diff = con_amount - btc_in_usd  # = 34.795

    btc_diff = usd_diff / bpi_data[x]

    wallet_balance['btc'] += btc_diff
    wallet_balance['usd'] -= usd_diff

print 'done'