from api_interface import coindesk
import string
import random

bpi_data = {}
wallet_balance = {}
dates = {}
h_bpi = coindesk.historical_bpi()

original_usd = 0
original_btc = 2

wallet_balance['usd'] = original_usd
wallet_balance['btc'] = original_btc

con_amount = 8789.2776  # the consistent amount in USD = 2 BTC at beginning
# con_amount = int(con_amount)

log = open('logs/simple_algorithm_' + ''.join(
    random.choice(string.ascii_uppercase + string.digits) for _ in range(5)) + '.txt', 'w')

bpi_data = h_bpi.get_bpi('2017-10-01', '2017-12-31')  # use format YYYY-MM-DD;

# print(bpi_data['bpi'])

# for x in bpi_data['bpi']:
#     print bpi_data['bpi'][x]

dates = bpi_data.keys()

y = 0

int(y)

for x in bpi_data:
    if (y == 0):
        z = x

    btc_in_usd = wallet_balance['btc'] * bpi_data[x]

    usd_diff = con_amount - btc_in_usd # = -18.9174
    btc_diff = usd_diff / bpi_data[x]

    wallet_balance['btc'] += btc_diff
    wallet_balance['usd'] -= usd_diff

    btc_in_usd = wallet_balance['btc'] * bpi_data[x]

    current_total_bal = wallet_balance['usd'] + btc_in_usd

    log.write('**************************************************\n')
    log.write('Date: ' + dates[y] + '\n')
    log.write('Current BPI: ' + str(bpi_data[x]) + ' USD\n')
    log.write('BTC Account Balance: ' + str(wallet_balance['btc']) + ' BTC or ' + str(btc_in_usd) + ' USD\n')
    log.write('USD Account Balance: ' + str(wallet_balance['usd']) + ' USD\n')
    log.write('Total Account Balance: ' + str(current_total_bal) + ' USD\n')
    log.write('Fixed Amount Difference: ' + str(btc_diff) + ' BTC or ' + str(usd_diff) + ' USD\n')
    # log.write('**************************************************\n')

    y+=1

    print'.',

# print(bpi_data[0])

original_balance = original_usd + original_btc * bpi_data[z]

usd_balance = wallet_balance['usd']

total_balance = usd_balance + con_amount

total_gain_loss = total_balance - (original_usd + original_btc * int(bpi_data[z]))

percentage = (abs(original_balance - total_balance) / (original_balance + total_balance) / 2) * 100

log.write('**************************************************\n')
log.write('/////////////////////Summary//////////////////////\n')
log.write('**************************************************\n')
log.write('Total Account Balance: ' + str(total_balance) + ' USD\n')
log.write('Total Balance of BTC Wallet: ' + str(wallet_balance['btc']) + ' BTC\n')
log.write('Total Balance of USD Wallet: ' + str(wallet_balance['usd']) + ' USD\n')
log.write('Total Gain or Loss: ' + str(total_gain_loss) + ' USD\n')
log.write('Percentage of Change: ' + str(percentage) + '%\n')
log.write('**************************************************\n')
log.write('//////////////////////////////////////////////////\n')
log.write('**************************************************')

print "\n\nStatus: program completed"