import requests
import base64
import hmac
import hashlib
from hashlib import sha384
import json
import random
import time
import uuid
import urllib2


class gemini_api_utils:

    def __init__(self):
        self.gemini_api_key = 'ReNfQfmDW0FNY1D7eLe3'
        self.gemini_api_secret = '4Kb4XHBpvLb2i7i3nptxAPN9QnAX'
        self.base_url = 'https://api.sandbox.gemini.com/v1'
        # self.symbol = symbol
        # self.amount = amount



    def getNonce(self):
        '''Returns a nonce created by the rounded time multiplied by 2'''
        nonce = int(round(time.time() * 2))
        return nonce


    def ticker(self, symbol):
        '''Returns as string the last USD exchange price for BTC on the Gemini exchange. Possible Symbols: btcusd ethusd ethbtc'''

        read = {}
        response = urllib2.urlopen("https://api.gemini.com/v1/pubticker/" + symbol)
        # return response

        read = json.loads(response.read())

        current_usd_price = read['last']
        return current_usd_price


    def createSig(self, param):
        '''Returns the signature for use in requesting to the Gemini API'''

        jsonSig = json.dumps(param)
        b64 = base64.b64encode(jsonSig)
        signature = hmac.new(self.gemini_api_secret, b64, hashlib.sha384).hexdigest()
        return b64, signature



    def getBalances(self, symbol):
        param = {}
        response_dict = {}

        url = self.base_url + '/balances'

        param['request'] = '/v1/balances'
        param['nonce'] = self.getNonce()

        b64, signature = self.createSig(param)

        headers = {
            'Content-Type': "text/plain",
            'Content-Length': "0",
            'X-GEMINI-APIKEY': self.gemini_api_key,
            'X-GEMINI-PAYLOAD': b64,
            'X-GEMINI-SIGNATURE': signature,
            'Cache-Control': "no-cache"
        }

        response = requests.request("POST", url, headers=headers)

        response_dict = json.loads(response.text)


        if(symbol):
            if (symbol == 'btc'):
                return response_dict[0]['amount']

            elif(symbol == 'usd'):
                return response_dict[1]['amount']

            elif(symbol == 'eth'):
                return response_dict[2]['amount']

            else:
                return "Symbol input error"

        else:
            return response_dict



    def new_order(self, symbol, amount, deal):
        '''Returns the non-modified response of a new order on the Gemini exchange.'''

        param = {}
        options = ['maker-or-cancel']
        url = self.base_url + '/order/new'

        param['request'] = '/v1/order/new'
        param['nonce'] = self.getNonce()
        #request specific headers
        param['symbol'] = symbol
        param['amount'] = amount
        param['price'] = self.ticker('btcusd')
        param['type'] = 'exchange limit'
        param['side'] = deal
        param['options'] = options

        b64, signature = self.createSig(param)

        headers = {
            'Content-Type': "text/plain",
            'Content-Length': "0",
            'X-GEMINI-APIKEY': self.gemini_api_key,
            'X-GEMINI-PAYLOAD': b64,
            'X-GEMINI-SIGNATURE': signature,
            'Cache-Control': "no-cache"
        }

        response = requests.request("POST", url, headers=headers)

        return response
