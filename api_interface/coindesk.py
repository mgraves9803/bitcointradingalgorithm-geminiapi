import requests
import json
from collections import OrderedDict

class historical_bpi:
    def __init__(self):
        self.base_url = 'https://api.coindesk.com/v1/bpi/historical/close.json'

    def get_bpi(self, start_date, end_date):
        '''Returns the daily Bitcoin Price index for every day between startDate and endDate in json format'''

        response = {}

        raw_response = requests.get(self.base_url + '?start=' + start_date + '&end=' + end_date)
        response = json.loads(raw_response.text.decode('utf-8'), object_pairs_hook=OrderedDict)




        return response['bpi']